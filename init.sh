#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Project name need to be provided"
    exit 1
fi

echo "Replacing projet name placeholder"
find . -path ./webapp/node_modules -prune -o \
  '(' -name "*.java" -o -name "*.yml" -o -name "*.sql" -o -name "*.xml" \
 -o -name "*.ts" -o -name "*.json" -o -name "*.html" -o -name "Dockerfile" ')' \
 -type f -exec sed -i "s/myapp/$1/g" {} +

 echo "Changing java package directories"
mv api/src/main/java/fr/afradet/myapp api/src/main/java/fr/afradet/"$1"
mv api/src/test/java/fr/afradet/myapp api/src/test/java/fr/afradet/"$1"
