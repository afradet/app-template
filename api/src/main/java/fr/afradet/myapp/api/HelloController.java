package fr.afradet.myapp.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

  @GetMapping("/hello")
  public Greeting greeting() {
    return new Greeting("Hello World!!!");
  }

  public record Greeting(String message) {}
}
